use super::schema::{users, users::dsl::*};
use super::DBType;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Queryable, Serialize, Deserialize)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "users"]
pub struct UserReq {
    pub username: String,
    pub password: String,
}

impl UserReq {
    pub fn register(&self, conn: &DBType) -> QueryResult<User> {
        diesel::insert_into(users::table)
            .values(self)
            .get_result::<User>(conn)
    }

    pub fn login(&self, conn: &DBType) -> QueryResult<User> {
        users
            .filter(username.eq(&self.username))
            .filter(password.eq(&self.password))
            .first(conn)
    }
}
