use super::models::UserReq;
use super::ConnectionPool;
use std::convert::Infallible;
use warp::Filter;

pub fn user_filters(
    pool: ConnectionPool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::reject::Rejection> + Clone {
    let user_path = warp::post()
        .and(warp::path("users"))
        .and(warp::body::json())
        .and(warp::any().map(move || pool.clone()));

    let register = user_path
        .clone()
        .and(warp::path("register"))
        .and_then(register_user);

    let login = user_path
        .clone()
        .and(warp::path("login"))
        .and_then(login_user);

    login.or(register)
}

async fn login_user(user: UserReq, db: ConnectionPool) -> Result<impl warp::Reply, Infallible> {
    let conn = db.get().unwrap();

    let usr = user.login(&conn).unwrap();
    Ok(warp::reply::json(&usr))
}

async fn register_user(user: UserReq, db: ConnectionPool) -> Result<impl warp::Reply, Infallible> {
    let conn = db.get().unwrap();

    let new_user = user.register(&conn).expect("Couldn't register user");
    Ok(warp::reply::json(&new_user))
}
