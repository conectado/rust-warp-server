#[macro_use]
extern crate diesel;

use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};
use dotenv::dotenv;

pub mod filters;
pub mod models;
pub mod schema;

pub type DBType = PgConnection;
pub type ConnectionPool = Pool<ConnectionManager<DBType>>;

fn get_connection_pool() -> ConnectionPool {
    let db_url =
        std::env::var("DATABASE_URL").expect("Please setup DATABASE_URL in .env in root folder");
    let manager = ConnectionManager::<PgConnection>::new(db_url);
    r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to build db connection pool")
}

#[tokio::main]
async fn main() {
    dotenv().ok();
    let pool = get_connection_pool();
    let routes = filters::user_filters(pool.clone());

    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await;
}
